# Baseline benchmark problem

## Description

STREAM: Sustainable Memory Bandwidth in High Performance Computers is
a standard benchmark to measure available memory bandwidth. For NESAP
the benchmark problem will be measured with the triad operation on
distributed 2TB arrays. For this example, the fixed array size
corresponds to the case where there is fixed problem size of interest.

```C
#pragma parallel for
for (i =0; i<N; i++) { 
    a[i] = b[i] + c[i] * SCALAR;
}
```

References:

* https://www.cs.virginia.edu/stream/
* https://www.cs.virginia.edu/stream/FTP/Code/

## Figure of Merit

Aggregate triad bandwidth (MB/s) with 2 TB arrays.

# Measurement on Edison

The measurement on Edison will be done with 128 nodes. 

Edison has 64 GB per node of DDR (~10GB for the OS image), or 8.192 TB
aggregate DDR for 128 nodes.

Stream triad requires 3 STREAM_TYPE arrays of STREAM_ARRAY_SIZE,
elements distributed evenly across all ranks.

The array size is set in ./src/Makefile with the following cpp
definition:

STREAM_ARRAY_SIZE=250000000000

which corresponds to

3 * 250000000000 * 8 bytes = 6 TB

aggregate memory used by the arrays.

No additional input is needed for this benchmark.

```shell
edison$ cd src && make clean && make && cd ../
edison$ cd ./stream-2TB
edison$ sbatch run-edison.sh
Submitted batch job 12665295
edison$ ./figure-of-merit.sh stream-2TB-edison.12665295.out
9212810.6 MB/s
```

# Measurement on Cori Haswell

Cori Haswell nodes have 128 GB of DDR memory. On this architecture the
same 2TB problem can be run on fewer nodes. The measurement on Cori
Haswell will be done with 64 nodes.

```shell
cori$ cd src && make clean && make && cd ../
cori$ cd ./stream-2TB
cori$ sbatch run-cori-haswell.sh
Submitted batch job 19672721
cori$ ./figure-of-merit.sh stream-2TB-cori-haswell.19672721.out
5791673.7 MB/s
```

# Epilog

These figures of merit will be used to compute a system throughput
metric, such as
[SSI](https://www.nersc.gov/research-and-development/benchmarking-and-workload-characterization/ssi/)

In SSI the relevant term for a given code is `cUS`, where:

* `c` is a capability factor to account for additional physics or weak
  scaling.  
* `U= (n_ref / n) * (N / N_ref)` is a utilization factor
  where: 
  * `n_ref` is the number of nodes used in reference system 
  * `n` is the number of nodes used by the application 
  * `N_ref` is the total number of nodes in the reference platform 
  * `N` is the total number of nodes 
* `S = (t_ref / t) or (FOM / FOM_ref)` is a speedup
  factor, where shorter times or higher figure of merit are the goals

## Example

Taking edison as the reference platform:

* `c=1`
* `U=(128/64) * (2388/5586)`
* `S=(5791673.7 / 9212810.6)`