#!/bin/bash
#SBATCH --job-name=stream-2TB
#SBATCH --output="stream-2TB-edison.%j.out"
#SBATCH --time=3
#SBATCH --qos=debug
#SBATCH --nodes=128
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=2

export OMP_NUM_THREADS=1
sbcast ../src/stream_mpi /tmp/stream_mpi
srun --cpu-bind=cores /tmp/stream_mpi
