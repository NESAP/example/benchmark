#!/bin/bash
#SBATCH --constraint=haswell
#SBATCH --job-name=stream-2TB
#SBATCH --output="stream-2TB-cori-haswell.%j.out"
#SBATCH --time=20
#SBATCH --qos=debug
#SBATCH --nodes=64
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2

export OMP_NUM_THREADS=1
sbcast ../src/stream_mpi /tmp/stream_mpi
srun --cpu-bind=cores /tmp/stream_mpi
